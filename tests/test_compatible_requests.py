import sys
import unittest
import unittest.mock

import test_server


def attempt_import():
    import requests  # noqa: F401


class TestCompatibleRequests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = test_server.MockHttpServer()
        cls.server.start()

    @classmethod
    def tearDownClass(cls):
        cls.server.end()

    def test_request_return_same_value_with_and_without_requests(
        self,
    ):
        # Execute
        url = "http://localhost:8000/api/get/json"

        import compatible_requests

        response_with_requests = compatible_requests.request("GET", url)

        # Clear imported cache
        if "requests" in sys.modules:
            del sys.modules["requests"]
        if "compatible_requests" in sys.modules:
            del sys.modules["compatible_requests"]

        with unittest.mock.patch.dict(
            "sys.modules",
            {"requests": None},
        ):
            import compatible_requests

            response_without_requests = compatible_requests.request(
                "GET", url
            )

            # Evaluate
            self.assertEqual(
                response_with_requests.status_code,
                response_without_requests.status_code,
            )
            self.assertEqual(
                response_with_requests.url,
                response_without_requests.url,
            )
            self.assertEqual(
                response_with_requests.encoding,
                response_without_requests.encoding,
            )
            self.assertEqual(
                response_with_requests.reason,
                response_without_requests.reason,
            )
            self.assertEqual(
                response_with_requests.content,
                response_without_requests.content,
            )
            self.assertEqual(
                response_with_requests.json()["message"],
                response_without_requests.json()["message"],
            )

    def test_error_request_return_same_value_with_and_without_requests(
        self,
    ):
        # Execute
        url = "http://localhost:8000/api/get/json/error"

        import compatible_requests

        response_with_requests = compatible_requests.request("GET", url)

        # Clear imported cache
        if "requests" in sys.modules:
            del sys.modules["requests"]
        if "compatible_requests" in sys.modules:
            del sys.modules["compatible_requests"]

        with unittest.mock.patch.dict(
            "sys.modules",
            {"requests": None},
        ):
            import compatible_requests

            response_without_requests = compatible_requests.request(
                "GET", url
            )

            # Evaluate
            self.assertEqual(
                response_with_requests.status_code,
                response_without_requests.status_code,
            )
            self.assertEqual(
                response_with_requests.url,
                response_without_requests.url,
            )
            # TODO(): Set encoding to read charset in html
            # self.assertEqual(
            #     response_with_requests.encoding,
            #     response_without_requests.encoding,
            # )
            self.assertEqual(
                response_with_requests.reason,
                response_without_requests.reason,
            )
            self.assertEqual(
                response_with_requests.content,
                response_without_requests.content,
            )
            self.assertRaises(Exception, response_with_requests.json)
            self.assertRaises(Exception, response_without_requests.json)

    def test_get_return_same_value_with_and_without_requests(
        self,
    ):
        # Execute
        url = "http://localhost:8000/api/get/json"

        import compatible_requests

        response_with_requests = compatible_requests.get(url)

        # Clear imported cache
        if "requests" in sys.modules:
            del sys.modules["requests"]
        if "compatible_requests" in sys.modules:
            del sys.modules["compatible_requests"]

        with unittest.mock.patch.dict(
            "sys.modules",
            {"requests": None},
        ):
            import compatible_requests

            response_without_requests = compatible_requests.get(url)

            # Evaluate
            self.assertEqual(
                response_with_requests.status_code,
                response_without_requests.status_code,
            )
            self.assertEqual(
                response_with_requests.url,
                response_without_requests.url,
            )
            self.assertEqual(
                str.upper(response_with_requests.encoding),
                str.upper(response_without_requests.encoding),
            )
            self.assertEqual(
                response_with_requests.reason,
                response_without_requests.reason,
            )
            self.assertEqual(
                response_with_requests.content,
                response_without_requests.content,
            )
            self.assertEqual(
                response_with_requests.json()["message"],
                response_without_requests.json()["message"],
            )

    def test_error_get_return_same_value_with_and_without_requests(
        self,
    ):
        # Execute
        url = "http://localhost:8000/test/api/get/json"

        import compatible_requests

        response_with_requests = compatible_requests.get(url)

        # Clear imported cache
        if "requests" in sys.modules:
            del sys.modules["requests"]
        if "compatible_requests" in sys.modules:
            del sys.modules["compatible_requests"]

        with unittest.mock.patch.dict(
            "sys.modules",
            {"requests": None},
        ):
            import compatible_requests

            response_without_requests = compatible_requests.request(
                "GET", url
            )

            # Evaluate
            self.assertEqual(
                response_with_requests.status_code,
                response_without_requests.status_code,
            )
            self.assertEqual(
                response_with_requests.url,
                response_without_requests.url,
            )
            # TODO(): Set encoding to read charset in html
            # self.assertEqual(
            #     response_with_requests.encoding,
            #     response_without_requests.encoding,
            # )
            self.assertEqual(
                response_with_requests.reason,
                response_without_requests.reason,
            )
            self.assertEqual(
                response_with_requests.content,
                response_without_requests.content,
            )
            self.assertRaises(Exception, response_with_requests.json)
            self.assertRaises(Exception, response_without_requests.json)

    def test_post_return_same_value_with_and_without_requests(
        self,
    ):
        # Execute
        url = "http://localhost:8000/api/post/json"

        import compatible_requests

        response_with_requests = compatible_requests.post(url)

        # Clear imported cache
        if "requests" in sys.modules:
            del sys.modules["requests"]
        if "compatible_requests" in sys.modules:
            del sys.modules["compatible_requests"]

        with unittest.mock.patch.dict(
            "sys.modules",
            {"requests": None},
        ):
            import compatible_requests

            response_without_requests = compatible_requests.post(url)

            # Evaluate
            self.assertEqual(
                response_with_requests.status_code,
                response_without_requests.status_code,
            )
            self.assertEqual(
                response_with_requests.url,
                response_without_requests.url,
            )
            self.assertEqual(
                str.upper(response_with_requests.encoding),
                str.upper(response_without_requests.encoding),
            )
            self.assertEqual(
                response_with_requests.reason,
                response_without_requests.reason,
            )
            self.assertEqual(
                response_with_requests.content,
                response_without_requests.content,
            )
            self.assertEqual(
                response_with_requests.json()["message"],
                response_without_requests.json()["message"],
            )

    def test_post_error_return_same_value_with_and_without_requests(
        self,
    ):
        # Execute
        url = "http://localhost:8000/test/api/post/json"

        import compatible_requests

        response_with_requests = compatible_requests.post(url)

        # Clear imported cache
        if "requests" in sys.modules:
            del sys.modules["requests"]
        if "compatible_requests" in sys.modules:
            del sys.modules["compatible_requests"]

        with unittest.mock.patch.dict(
            "sys.modules",
            {"requests": None},
        ):
            import compatible_requests

            response_without_requests = compatible_requests.request(
                "GET", url
            )

            # Evaluate
            self.assertEqual(
                response_with_requests.status_code,
                response_without_requests.status_code,
            )
            self.assertEqual(
                response_with_requests.url,
                response_without_requests.url,
            )
            # TODO(): Set encoding to read charset in html
            # self.assertEqual(
            #     response_with_requests.encoding,
            #     response_without_requests.encoding,
            # )
            self.assertEqual(
                response_with_requests.reason,
                response_without_requests.reason,
            )
            self.assertEqual(
                response_with_requests.content,
                response_without_requests.content,
            )
            self.assertRaises(Exception, response_with_requests.json)
            self.assertRaises(Exception, response_without_requests.json)


if __name__ == "__main__":
    unittest.main()
