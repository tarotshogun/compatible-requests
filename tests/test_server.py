import http.server
import threading


class MockServerRequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/api/get/json":
            self.send_response(200, "OK")
            self.send_header(
                "Content-Type", "application/json; charset=utf-8"
            )
            self.end_headers()
            response_content = '{"message": "This is a mock response"}'
            self.wfile.write(response_content.encode("utf-8"))
        else:
            self.send_error(404, "Not Found: {}".format(self.path))

    def do_POST(self):
        if self.path == "/api/post/json":
            self.send_response(200, "OK")
            self.send_header(
                "Content-Type", "application/json; charset=utf-8"
            )
            self.end_headers()
            response_content = '{"message": "This is a mock response"}'
            self.wfile.write(response_content.encode("utf-8"))
        else:
            self.send_error(404, "Not Found: {}".format(self.path))


class MockHttpServer:
    def start(self):
        mock_server_port = 8000
        self.mock_server = http.server.HTTPServer(
            ("localhost", mock_server_port), MockServerRequestHandler
        )
        self.mock_server_thread = threading.Thread(
            target=self.mock_server.serve_forever
        )
        self.mock_server_thread.setDaemon(True)
        self.mock_server_thread.start()

    def end(self):
        self.mock_server.shutdown()
        self.mock_server_thread.join()
        self.mock_server.server_close()
