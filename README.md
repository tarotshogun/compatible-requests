# Compatible requests

このモジュールは[requests](https://github.com/psf/requests)のラッパーモジュールです。
[requests](https://github.com/psf/requests)をインポートしていない、もしくはインポートできない環境において、[urllib](https://docs.python.org/ja/3/library/urllib.html)を使うように切り替えることで、スクリプトの互換性を保つことを目的としています。

```python
>>> import compatible_requests
>>> compatible_requests.get('https://httpbin.org/basic-auth/user/pass', auth=('user', 'pass'))
>>> r.status_code
200
>>> r.headers['content-type']
'application/json; charset=utf8'
>>> r.encoding
'utf-8'
>>> r.text
'{"authenticated": true, ...'
>>> r.json()
{'authenticated': True, ...}
```

## インストール方法

pipのeditableモードを使ってインストールできます。

```bash
pip install -e .
```

もしくは、`/src`配下の`compatible_requests`ディレクトリを実行するpythonスクリプトと同じ場所に配置してください。

## アンインストール方法

```bash
pip uninstall compatible_requests
```

## 問い合わせ先

質問、ご指摘等あれば以下にご連絡ください。

[Compatible requests](https://gitlab.com/tarotshogun/compatible-requests/-/issues)

## ロードマップ

以下の機能追加を計画しています。

- docstringのメンテナンス
- DELETEメソッドの実装
- テストコードのリファクタリング
- テストケースの拡充
- CIによるテストの自動化
- コントリビューターを対象にしたドキュメントの追加

## コントリビュート

第三者のプロジェクトの変更を歓迎します。プルリクエストを作成してあなたのコードを提案してください。

コーディング規約や品質を担保するためにテストを用意しています。テストを実行するために[tox](https://tox.wiki/en/latest/)のインストールが必要です。以下のコマンドでテストを実行できます。

```bash
tox
```

toxを使えない場合は以下のコードでunittestを実行できます。

```bash
python -m unittest discover tests
```
