import json


class Response:
    """The :class:`Response <Response>` object, which contains a
    server's response to an HTTP request.
    """

    __attrs__ = [
        "status_code",
        "headers",
        "url",
        "encoding",
        "reason",
        "content",
    ]

    def __init__(self):
        self.status_code = None
        self.headers = []
        self.url = None
        self.encoding = None
        self.reason = None
        self.content = None

    @property
    def text(self):
        """Content of the response, in unicode."""
        encoding = ""
        if "charset" in self.headers.get_content_charset():
            encoding = self.headers.get_content_charset()
        else:
            encoding = "utf-8"  # default encoding

        response_content = self.content
        return response_content.decode(encoding)

    def json(self, **kwargs):
        r"""Returns the json-encoded content of a response, if any.

        :param \*\*kwargs: Optional arguments that ``json.loads`` takes.
        :raises json.decoder.JSONDecodeError: If the response body does
            not contain valid json.
        """
        data = json.loads(self.text, **kwargs)
        return data


try:
    import requests

    def request(method, url, **kwargs):
        """Constructs and sends a :class:`Request <Request>`.

        :param method: method for the new :class:`Request` object: ``GET``,
            ``OPTIONS``, ``HEAD``, ``POST``, ``PUT``, ``PATCH``, or ``DELETE``.
        :param url: URL for the new :class:`Request` object.
        :param params: (optional) Dictionary, list of tuples or bytes to send
            in the query string for the :class:`Request`.
        :param data: (optional) Dictionary, list of tuples, bytes, or file-like
            object to send in the body of the :class:`Request`.
        :param json: (optional) A JSON serializable Python object to send in
            the body of the :class:`Request`.
        :param headers: (optional) Dictionary of HTTP Headers to send with the
            :class:`Request`.
        :param cookies: (optional) Dict or CookieJar object to send with the
            :class:`Request`.
        :param files: (optional) Dictionary of ``'name': file-like-objects``
            (or ``{'name': file-tuple}``) for multipart encoding upload.
            ``file-tuple`` can be a 2-tuple ``('filename', fileobj)``, 3-tuple
            ``('filename', fileobj, 'content_type')`` or a 4-tuple
            ``('filename', fileobj, 'content_type', custom_headers)``,
            where ``'content-type'`` is a string defining the content type of
            the given file and ``custom_headers`` a dict-like object
            containing additional headers to add for the file.
        :param auth: (optional)
            Auth tuple to enable Basic/Digest/Custom HTTP Auth.
        :param timeout: (optional)
            How many seconds to wait for the server to send data
            before giving up, as a float, or a :ref:`(connect timeout, read
            timeout) <timeouts>` tuple.
        :type timeout: float or tuple
        :param allow_redirects: (optional) Boolean.
            Enable/disable GET/OPTIONS/POST/PUT/PATCH/DELETE/HEAD redirection.
            Defaults to ``True``.
        :type allow_redirects: bool
        :param proxies: (optional)
            Dictionary mapping protocol to the URL of the proxy.
        :param verify: (optional)
            Either a boolean, in which case it controls whether we verify
            the server's TLS certificate, or a string, in which case it must
            be a path to a CA bundle to use. Defaults to ``True``.
        :param stream: (optional)
            if ``False``, the response content will be immediately downloaded.
        :param cert: (optional) if String, path to ssl client cert file (.pem).
            If Tuple, ('cert', 'key') pair.
        :return: :class:`Response <Response>` object
        :rtype: requests.Response

        Usage::

        >>> import compatible_requests
        >>> req = compatible_requests.request('GET', 'https://httpbin.org/get')
        >>> req
        <Response [200]>
        """

        return requests.request(method, url, **kwargs)

    def get(url, params=None, **kwargs):
        r"""Sends a GET request.

        :param url: URL for the new :class:`Request` object.
        :param params: (optional) Dictionary, list of tuples or bytes to send
            in the query string for the :class:`Request`.
        :param \*\*kwargs: Optional arguments that ``requests.request`` takes.
        :return: :class:`Response <Response>` object
        :rtype: requests.Response
        """

        return requests.request("get", url, params=params, **kwargs)

    def post(url, data=None, body=None, **kwargs):
        r"""Sends a POST request.

        :param url: URL for the new :class:`Request` object.
        :param data: (optional) Dictionary, list of tuples, bytes, or file-like
            object to send in the body of the :class:`Request`.
        :param body: (optional) A JSON serializable Python object to send in
            the body of the :class:`Request`.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        :return: :class:`Response <Response>` object
        :rtype: requests.Response
        """

        return requests.request("post", url, data=data, json=body, **kwargs)

except ImportError:
    import urllib.error
    import urllib.parse
    import urllib.request

    def request(method, url, **kwargs):
        r"""Sends a GET request.

        :param url: URL for the new :class:`Request` object.
        :param params: (optional) Dictionary, list of tuples or bytes to send
            in the query string for the :class:`Request`.
        :param \*\*kwargs: Optional arguments that ``requests.request`` takes.
        :return: :class:`Response <Response>` object
        :rtype: requests.Response
        """
        method = method.upper()
        headers = kwargs.get("headers", {})
        data = kwargs.get("data", None)
        params = kwargs.get("params", None)
        timeout = kwargs.get("timeout", None)

        if params:
            url += "?" + urllib.parse.urlencode(params)

        if data:
            if isinstance(data, dict):
                data = urllib.parse.urlencode(data).encode("utf-8")
            elif isinstance(data, str):
                data = data.encode("utf-8")

        req = urllib.request.Request(url, data=data, method=method)

        for key, value in headers.items():
            req.add_header(key, value)

        try:
            with urllib.request.urlopen(req, timeout=timeout) as response:
                ret = Response()
                ret.url = response.url
                ret.status_code = response.status
                ret.headers = response.headers
                ret.encoding = response.headers.get_content_charset()
                ret.reason = response.reason
                ret.content = response.read()
                return ret
        except urllib.error.HTTPError as e:
            ret = Response()
            ret.url = e.url
            ret.status_code = e.code
            ret.headers = e.headers
            ret.reason = e.reason
            ret.content = e.fp.read()
            return ret
        except urllib.error.URLError as e:
            raise e from None

    def get(url, params=None, **kwargs):
        r"""Sends a GET request.

        :param url: URL for the new :class:`Request` object.
        :param params: (optional) Dictionary, list of tuples or bytes to send
            in the query string for the :class:`Request`.
        :param \*\*kwargs: Optional arguments that ``requests.request`` takes.
        :return: :class:`Response <Response>` object
        :rtype: requests.Response
        """

        return request("get", url, params=params, **kwargs)

    def post(url, data=None, json=None, **kwargs):
        r"""Sends a POST request.

        :param url: URL for the new :class:`Request` object.
        :param data: (optional) Dictionary, list of tuples, bytes, or file-like
            object to send in the body of the :class:`Request`.
        :param json: (optional) A JSON serializable Python object to send in
            the body of the :class:`Request`.
        :param \*\*kwargs: Optional arguments that ``request`` takes.
        :return: :class:`Response <Response>` object
        :rtype: requests.Response
        """

        return request("post", url, data=data, json=json, **kwargs)
